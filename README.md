# wordle like puzzle
inspired by [wordle](https://www.powerlanguage.co.uk/wordle/) and [hello-wordl](https://github.com/lynn/hello-wordl)  
built with [Svelte](https://svelte.dev)  

## not an exact clone
- unlimited guesses, unlimited plays
- doesn't restrict guesses to real words
